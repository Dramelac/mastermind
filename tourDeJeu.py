def Play(nb_Color,len_Guess):                                                   #input players
    error = True
    Choix = "0"
    Choix_list = [0]*len_Guess
    while error == True or 0 <= int(Choix) >= 10 ** len_Guess:
        error = False
        while len_Guess != len(Choix):                                          #check len input
            Choix = input("Entrer la suite de couleur à proposer : ")
        for i in range(0,len_Guess):
            if 0 < int(Choix[i]) > nb_Color:                                    #check color code
                print("Erreur : color code not correct")
                Choix = "0"
                error = True
                break
            Choix_list[i] = int(Choix[i])                                       #save input into list
    return Choix_list

def CheckPlay(Choix_list,Guess):
    same = 0    #pions bien placé
    place = 0   #pions présent mais mal placé
    Temp1 = list(Choix_list)                                                    #copy list temp
    Temp2 = list(Guess)
    for x in range(len(Guess)):                                                 #check exact position
        if Temp1[x] == Temp2[x]:
            same += 1

    for a in range(len(Guess)):                                                 #check existing color
        for b in range(len(Guess)):
            if Temp1[a] == Temp2[b]:
                place += 1
                Temp1[a] = 'A'
                Temp2[b] = 'B'

    place -= same                                                               #soustracting result

    return same,place


def affichage_result(same,place):                                               #score on screen
    print(same,"bien placé(s), et ",place,"mal placé(s)")


def affichage_guess(list):
    print("Perdu il fallair trouver : ",end='')
    for x in list:
        print(x,end='')