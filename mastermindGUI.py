import pygame,sys
from pygame.locals import *
from initialisations import *
from tourDeJeu import *


# RGB Colors
BLACK = (0, 0, 0)
GREEN = (30, 200, 30)
BLUE = (30, 30, 240)
ORANGE = (220, 150, 35)
RED = (220, 35, 35)
YELLOW = (250, 255, 66)
PINK = (220, 45, 210)
WHITE = (255, 255, 255)

LARGEUR = 700
HAUTEUR = 750

FPS = 30

# Init and load stuff
fpsClock = pygame.time.Clock()

maSurface = pygame.display.set_mode((LARGEUR, HAUTEUR))
pygame.display.set_caption('Mastermind | Mini-Projet 1ADS')

# Images
background = pygame.image.load("data/background.png")
mastermind = pygame.image.load("data/mastermind.png")
pseudo_image = pygame.image.load('data/pseudo.png')
win_img = pygame.image.load("data/win.png")
loose_img = pygame.image.load("data/loose.png")
ligne = pygame.image.load('data/ligne.png')
blood = pygame.image.load('data/blood.png')
ligne_rect = ligne.get_rect()

# Sounds
gameSounds = {}
pygame.mixer.init(44100, -16, 2, 2048)
gameSounds['game over'] = pygame.mixer.Sound('sounds/win_game.wav')
gameSounds['end game'] = pygame.mixer.Sound('sounds/end_game.wav')
gameSounds['same'] = pygame.mixer.Sound('sounds/same.wav')
gameSounds['place'] = pygame.mixer.Sound('sounds/place.wav')
gameSounds['touch'] = pygame.mixer.Sound('sounds/touch.wav')
gameSounds['shot'] = pygame.mixer.Sound('sounds/magnum.wav')

# Init variable game
nb_Color = 6
len_Guess = 4
Choix_list = [0]*len_Guess
nb_Try = 10

# Init file (to save game)
resultPlay = [0]*20
gamePlay = [0]*40

try:
    mon_fichier = open('user.crypto', "r")
    mon_fichier.close()
except IOError:
    mon_fichier = open('user.crypto', "w")
    mon_fichier.close()

def quit(event):

    if event.type == QUIT:
        pygame.quit()
        sys.exit()

def GUIinit():  # Init game table

    pygame.init()
    maSurface.blit(background, (0, 0))
    maSurface.blit(mastermind, (450, 680))
    drawLigne()
    drawNumero()
    drawText("Solution :", 40, 680, 30)

def drawLigne(start=0, end=10):

    for x in range(start, end):
        maSurface.blit(ligne, (40, 70 + x*60))  # Draw table lines
        pygame.draw.circle(maSurface, BLACK, (395, 60*x+90), 7)  # Draw 4 circles for solutions (test)
        pygame.draw.circle(maSurface, BLACK, (395, 60*x+110), 7)
        pygame.draw.circle(maSurface, BLACK, (420, 60*x+90), 7)
        pygame.draw.circle(maSurface, BLACK, (420, 60*x+110), 7)

        for i in range(4):  # Draw circles
            pygame.draw.circle(maSurface,BLACK,(i*67+140, 60*x+100), 7)  # Circles for colors in game table
            pygame.draw.circle(maSurface,BLACK,(i*67+140, 720), 7)  # Solution circles

def drawNumero(start=0, end=10):  # Draw line number

    font = pygame.font.Font("data/ReliefGroteskBold.ttf",30)

    for x in range(start, end):
        numero = font.render(str(x+1), 1, WHITE)
        maSurface.blit(numero, (60, 85 + x*60))

def drawRecommencer(): # Draw restart button
    font = pygame.font.Font(None, 30)

    recommencer = font.render(str("Recommencer"), 1, WHITE)
    recommencer_rect = recommencer.get_rect()
    recommencer_rect.bottomleft = (500, 110)
    maSurface.blit(recommencer, recommencer_rect)

    return recommencer_rect

def drawEffacerLigne():  # Draw erase button

    font = pygame.font.Font(None, 30)

    effacerLigne = font.render(str("Effacer la ligne"), 1, WHITE)
    effacerLigne_rect = effacerLigne.get_rect()
    effacerLigne_rect.bottomleft = (500, 170)
    maSurface.blit(effacerLigne, effacerLigne_rect)

    return effacerLigne_rect

def drawTesterLigne():  # Draw test button

    font = pygame.font.Font(None, 30)

    testerLigne = font.render(str("Tester la ligne"), 1, WHITE)
    testerLigne_rect = testerLigne.get_rect()
    testerLigne_rect.bottomleft = (500, 230)
    maSurface.blit(testerLigne, testerLigne_rect)

    return testerLigne_rect

def drawText(text, a, b, police=30):  # Draw in-game text

    font = pygame.font.Font(None, police)

    solution = font.render(str(text), 1, WHITE)
    maSurface.blit(solution, (a, b))

def drawSaveGame(): # Draw save game button

    font = pygame.font.Font(None, 30)

    saveGame = font.render(str("Sauvegarder la partie"), 1, WHITE)
    saveGame_rect = saveGame.get_rect()
    saveGame_rect.bottomleft = (465, 290)
    maSurface.blit(saveGame, saveGame_rect)

    return saveGame_rect

def drawPseudo(pseudo):

    font = pygame.font.Font(None, 20)
    font.set_italic(1)
    renderPseudo = font.render(str(pseudo), 1, WHITE)
    renderPseudo_rect = renderPseudo.get_rect(center=(570, 45))
    maSurface.blit(renderPseudo, renderPseudo_rect)

def affichierSolution(Guess):  # Draw final solution

    for x in range(len(Guess)):
        if Guess[x] == 1:
            TempColor = GREEN
        if Guess[x] == 2:
            TempColor = BLUE
        if Guess[x] == 3:
            TempColor = ORANGE
        if Guess[x] == 4:
            TempColor = RED
        if Guess[x] == 5:
            TempColor = YELLOW
        if Guess[x] == 6:
            TempColor = PINK
        pygame.draw.circle(maSurface, TempColor, (x*67+140, 720), 15)
    pygame.display.update()

def afficherResultat(same, place, ligne_number): # Display result after test (red, white)

    if same > 0:
        pygame.draw.circle(maSurface, RED, (395, 60*ligne_number+90), 7)
        gameSounds['same'].play()
        same -= 1
        pygame.display.update()
        pygame.time.delay(300)
    elif place > 0:
        pygame.draw.circle(maSurface, WHITE, (395, 60*ligne_number+90), 7)
        gameSounds['place'].play()
        place -= 1
        pygame.display.update()
        pygame.time.delay(300)

    if same > 0:
        pygame.draw.circle(maSurface, RED, (420, 60*ligne_number+90), 7)
        gameSounds['same'].play()
        same -= 1
        pygame.display.update()
        pygame.time.delay(300)
    elif place > 0:
        pygame.draw.circle(maSurface, WHITE, (420, 60*ligne_number+90), 7)
        gameSounds['place'].play()
        place -= 1
        pygame.display.update()
        pygame.time.delay(300)

    if same > 0:
        pygame.draw.circle(maSurface, RED, (395, 60*ligne_number+110), 7)
        gameSounds['same'].play()
        same -= 1
        pygame.display.update()
        pygame.time.delay(300)
    elif place > 0:
        pygame.draw.circle(maSurface, WHITE, (395, 60*ligne_number+110), 7)
        gameSounds['place'].play()
        place -= 1
        pygame.display.update()
        pygame.time.delay(300)

    if same > 0:
        pygame.draw.circle(maSurface, RED, (420, 60*ligne_number+110), 7)
        gameSounds['same'].play()
    elif place > 0:
        pygame.draw.circle(maSurface, WHITE, (420, 60*ligne_number+110), 7)
        gameSounds['place'].play()

    pygame.display.update()

def drawColor():  # Init colors

    color_rect_green = pygame.draw.circle(maSurface,GREEN,(75,35),15)
    color_rect_blue = pygame.draw.circle(maSurface,BLUE,(140,35),15)
    color_rect_orange = pygame.draw.circle(maSurface,ORANGE,(205,35),15)
    color_rect_red = pygame.draw.circle(maSurface,RED,(275,35),15)
    color_rect_yellow = pygame.draw.circle(maSurface,YELLOW,(345,35),15)
    color_rect_pink = pygame.draw.circle(maSurface,PINK,(410,35),15)

    return color_rect_green,color_rect_blue, color_rect_orange, color_rect_red, color_rect_yellow, color_rect_pink

def inputColor(color,ligne_number,colone_number):  # To put a color in game table

    gameSounds['touch'].play()
    pygame.draw.circle(maSurface,color,(colone_number*67+140, 60*ligne_number+100), 15)
    colone_number += 1
    pygame.display.update()

    return colone_number

def cryptage(word, keys):  # To encrypt a game before saving it

    if type(word) == list:
        word = list(word)

    key = [0]*len(keys)
    for i in range(len(keys)):
        key[i] = ord(keys[i])  # Ascii code

    i = 0
    final = [0]*len(word)
    for x in range(len(word)):
        final[x] = (word[x] + key[i]) % 6
        i += 1
        if i >= len(key):
            i = 0
    return final

def decrytage(word, keys):  # To decrypt a saved game before restoring it

    if type(word) == list:
        word = list(word)

    key = [0]*len(keys)
    for i in range(len(keys)):
        key[i] = ord(keys[i])   # Ascii code

    i = 0
    final = [0]*len(word)
    for x in range(len(word)):
        final[x] = (word[x] - key[i]) % 6
        i += 1
        if i >= len(key):
            i = 0
    return final

def writeSave(write):  # To write "write" data in a file

    mon_fichier = open('user.crypto', "a")
    for i in write:
        mon_fichier.write(str(i))
    mon_fichier.write("&")
    mon_fichier.close()

def loadSave(pseudo):  # To load data from a a saved game

    saveFile = open('user.crypto', 'r')
    for line in saveFile:
        if line.find(pseudo) == 0:
            lineLoading = line
            break
    x = 0
    colone_number = 0
    loadPseudo = [0]*10
    loadGuess = [0]*4
    loadResult = [0]*20
    loadPlay = [0]*40
    loadPlace = [0]*4
    StatePseudo = False
    StateTurn = False
    StateGuess = False
    StateResult = False
    StatePlay = False
    StatePlace = False
    try:
        for letter in range(len(lineLoading)):

            if not StatePseudo and lineLoading[letter] != '&':
                loadPseudo[letter] = lineLoading[letter]
            elif not StatePseudo and lineLoading[letter] == '&':
                StatePseudo = True
            else:

                if not StateTurn and lineLoading[letter] != '&':
                    Turn = int(lineLoading[letter])
                elif not StateTurn and lineLoading[letter] == '&':
                    StateTurn = True
                else:

                    if not StateGuess and lineLoading[letter] != '&':
                        loadGuess[x] = int(lineLoading[letter])
                        x += 1
                    elif not StateGuess and lineLoading[letter] == '&':
                        StateGuess = True
                        x = 0
                    else:

                        if not StateResult and lineLoading[letter] != '&':
                            loadResult[x] = int(lineLoading[letter])
                            x += 1
                        elif not StateResult and lineLoading[letter] == '&':
                            StateResult = True
                            x = 0
                        else:

                            if not StatePlay and lineLoading[letter] != '&':
                                loadPlay[x] = int(lineLoading[letter])
                                x += 1
                            elif not StatePlay and lineLoading[letter] == '&':
                                StatePlay = True
                                x = 0
                            else:

                                if not StatePlace and (lineLoading[letter] == '0' or lineLoading[letter] == '&'):
                                    colone_number = x
                                    StatePlace = True
                                elif not StatePlace and lineLoading[letter] != '&':
                                    loadPlace[x] = int(lineLoading[letter])
                                    x += 1
                                elif StatePlace:
                                    removeSave(pseudo, Turn, loadGuess, loadResult, loadPlay, loadPlace)
                                    return Turn, decrytage(loadGuess, pseudo), loadResult, loadPlay, loadPlace, colone_number
    except UnboundLocalError:
        print("Fatal Error :  Save file not found with the pseudo :", pseudo)
        pygame.quit()
        sys.exit()

def removeSave(pseudo, Turn, loadGuess, loadResult, loadPlay, loadPlace):  # To remove data from a save game file

    checkGuess, checkResult, checkPlay, checkPlace = "", "", "", ""

    for x in loadGuess:
        checkGuess += str(x)
    for x in loadResult:
        checkResult += str(x)
    for x in loadPlay:
        checkPlay += str(x)
    for x in loadPlace:
        checkPlace += str(x)

    file = open("user.crypto", "r")
    lines = file.readlines()
    file.close()

    file = open("user.crypto", "w")
    for line in lines:
        if line != pseudo+"&"+str(Turn)+"&"+checkGuess+"&"+checkResult+"&"+checkPlay+"&"+checkPlace+"&~"+"\n":
            file.write(line)
    file.close()

def saveGame(pseudo, ligne_number, Guess, resultPlay, gamePlay, Choix_list):  # To save game

    writeSave(pseudo)

    writeSave(str(ligne_number))

    writeSave(cryptage(Guess, pseudo))

    writeSave(resultPlay)

    writeSave(gamePlay)

    writeSave(Choix_list)

    mon_fichier = open('user.crypto', "a")
    mon_fichier.write("~\n")
    mon_fichier.close()

def entrerPseudo():  # Ask's for users name and then displays it

    pseudo = ""
    font = pygame.font.Font(None, 50)

    while True:

        for event in pygame.event.get():

            quit(event)

            if event.type == KEYDOWN:
                if 32 <= event.key < 123:  # input name using unicode characters
                    pseudo += event.unicode
                    if len(pseudo) > 10:  # Users name is limited to 10 characters
                        pseudo = pseudo[:-1]
                elif event.key == K_BACKSPACE:
                    pseudo = pseudo[:-1]
                elif event.key == K_RETURN and pseudo != "":
                    return pseudo

        maSurface.blit(pseudo_image, (150, 500))
        pseudo_block = font.render(pseudo, True, (255, 255, 255))
        pseudo_block_rect = pseudo_block.get_rect(center=(350, 530))
        maSurface.blit(pseudo_block, pseudo_block_rect)
        pygame.display.flip()

def drawLoadSave(loadResult, loadPlay, ligne_number, Choix_list):

    # Draw played lines
    for line in range(ligne_number):
        for play in range(4*line, 4*line+4):
            if loadPlay[play] == 1:
                pygame.draw.circle(maSurface, GREEN, ((play % 4)*67+140, 60*line+100), 15)
            elif loadPlay[play] == 2:
                pygame.draw.circle(maSurface, BLUE, ((play % 4)*67+140, 60*line+100), 15)
            elif loadPlay[play] == 3:
                pygame.draw.circle(maSurface, ORANGE, ((play % 4)*67+140, 60*line+100), 15)
            elif loadPlay[play] == 4:
                pygame.draw.circle(maSurface, RED, ((play % 4)*67+140, 60*line+100), 15)
            elif loadPlay[play] == 5:
                pygame.draw.circle(maSurface, YELLOW, ((play % 4)*67+140, 60*line+100), 15)
            elif loadPlay[play] == 6:
                pygame.draw.circle(maSurface, PINK, ((play % 4)*67+140, 60*line+100), 15)

        # Init draw result
        same = loadResult[2*line]
        place = loadResult[2*line+1]

        # Draw result
        if same > 0:
            pygame.draw.circle(maSurface, RED, (395, 60*line+90), 7)
            same -= 1
        elif place > 0:
            pygame.draw.circle(maSurface, WHITE, (395, 60*line+90), 7)
            place -= 1

        if same > 0:
            pygame.draw.circle(maSurface, RED, (420, 60*line+90), 7)
            same -= 1
        elif place > 0:
            pygame.draw.circle(maSurface, WHITE, (420, 60*line+90), 7)
            place -= 1

        if same > 0:
            pygame.draw.circle(maSurface, RED, (395, 60*line+110), 7)
            same -= 1
        elif place > 0:
            pygame.draw.circle(maSurface, WHITE, (395, 60*line+110), 7)
            place -= 1

        if same > 0:
            pygame.draw.circle(maSurface, RED, (420, 60*line+110), 7)
        elif place > 0:
            pygame.draw.circle(maSurface, WHITE, (420, 60*line+110), 7)

    # Draw Choix_list (colors player have played but haven't tested yet)
    for play in range(4):
        if Choix_list[play] == 1:
            pygame.draw.circle(maSurface, GREEN, ((play)*67+140, 60*ligne_number+100), 15)
        elif Choix_list[play] == 2:
            pygame.draw.circle(maSurface, BLUE, ((play)*67+140, 60*ligne_number+100), 15)
        elif Choix_list[play] == 3:
            pygame.draw.circle(maSurface, ORANGE, ((play)*67+140, 60*ligne_number+100), 15)
        elif Choix_list[play] == 4:
            pygame.draw.circle(maSurface, RED, ((play)*67+140, 60*ligne_number+100), 15)
        elif Choix_list[play] == 5:
            pygame.draw.circle(maSurface, YELLOW, ((play)*67+140, 60*ligne_number+100), 15)
        elif Choix_list[play] == 6:
            pygame.draw.circle(maSurface, PINK, ((play)*67+140, 60*ligne_number+100), 15)

def playgame(notSaveGame, Choix_list, pseudo):

        # Game loop
        while True:

            GUIinit()
            drawPseudo(pseudo)
            color_rect_green, color_rect_blue, color_rect_orange, color_rect_red, color_rect_yellow, color_rect_pink = drawColor()
            saveGame_rect = drawSaveGame()
            recommencer_rect = drawRecommencer()
            effacerLigne_rect = drawEffacerLigne()
            testerLigne_rect = drawTesterLigne()
            color = None
            lineNumber = 1

            if notSaveGame:  # Start a new game
                ligne_number, colone_number, same = 0, 0, 0
                Guess = init_Guess(nb_Color, len_Guess) # Random Guess (random colors combination)

            else:  # Load a game
                same = 0
                ligne_number, Guess, loadResult, loadPlay, Choix_list, colone_number = loadSave(pseudo)
                drawLoadSave(loadResult, loadPlay, ligne_number, Choix_list)
                notSaveGame = True

            pygame.display.update()

            Continuer = True

            # Current game loop
            while Continuer and ligne_number < 10 and same != 4:

                for event in pygame.event.get():
                    quit(event)

                    # Test mouse click
                    if event.type == MOUSEBUTTONDOWN and event.button == 1:

                            if color_rect_green.collidepoint(event.pos) and colone_number <= 3:
                                color = GREEN
                                Choix_list[colone_number] = 1
                                colone_number = inputColor(color, ligne_number, colone_number)

                            if color_rect_blue.collidepoint(event.pos) and colone_number <= 3:
                                color = BLUE
                                Choix_list[colone_number] = 2
                                colone_number = inputColor(color, ligne_number, colone_number)

                            if color_rect_orange.collidepoint(event.pos) and colone_number <= 3:
                                color = ORANGE
                                Choix_list[colone_number] = 3
                                colone_number = inputColor(color, ligne_number, colone_number)

                            if color_rect_red.collidepoint(event.pos) and colone_number <= 3:
                                color = RED
                                Choix_list[colone_number] = 4
                                colone_number = inputColor(color, ligne_number, colone_number)

                            if color_rect_yellow.collidepoint(event.pos) and colone_number <= 3:
                                color = YELLOW
                                Choix_list[colone_number] = 5
                                colone_number = inputColor(color, ligne_number, colone_number)

                            if color_rect_pink.collidepoint(event.pos) and colone_number <= 3:
                                color = PINK
                                Choix_list[colone_number] = 6
                                colone_number = inputColor(color, ligne_number, colone_number)

                            if recommencer_rect.collidepoint(event.pos):
                                Continuer = False
                                # Quit current game loop / start a new game

                            if effacerLigne_rect.collidepoint(event.pos):
                                colone_number = 0
                                Choix_list = [0] * len_Guess
                                drawLigne(ligne_number, ligne_number+1)
                                drawNumero(ligne_number, ligne_number+1)
                                # Erase line

                            if testerLigne_rect.collidepoint(event.pos):

                                if colone_number == 4:
                                    same, place = CheckPlay(Choix_list, Guess)  # Test current line

                                    resultPlay[ligne_number*2] = same
                                    resultPlay[ligne_number*2+1] = place

                                    for x in range(len_Guess):
                                        gamePlay[4*ligne_number+x] = Choix_list[x]

                                    Choix_list = [0] * 4

                                    afficherResultat(same, place, ligne_number)

                                    ligne_number += 1
                                    colone_number = 0

                                else:
                                    print("Error, not enough color place...")

                            if saveGame_rect.collidepoint(event.pos):
                                saveGame(pseudo, ligne_number, Guess, resultPlay, gamePlay, Choix_list)
                                pygame.quit()
                                sys.exit()


                            pygame.display.update()

                fpsClock.tick(FPS)

            affichierSolution(Guess)

            # Win game
            if same == 4:
                maSurface.blit(win_img, (440, 300))
                drawText("Bravo !", 515, 500, 50)
                pygame.display.update()
                gameSounds['game over'].play()

            # Loose game
            else:
                maSurface.blit(loose_img, (440, 300))
                drawText("Dommage ...", 480, 500, 50)
                pygame.display.update()
                gameSounds['end game'].play()
                pygame.time.delay(3000)
                gameSounds['shot'].play()
                maSurface.blit(blood, (290, 100))
                pygame.display.update()

            Continuer = True

            # Loop to display solution + restart
            while Continuer:
                for event in pygame.event.get():

                    quit(event)

                    if event.type == MOUSEBUTTONDOWN and event.button == 1:
                        Continuer = False

                fpsClock.tick(FPS)


pygame.init()
maSurface.blit(background, (0, 0))
maSurface.blit(mastermind, (230, 50))
font = pygame.font.Font(None, 50)
nouvellePartie = font.render(str("Jouer une nouvelle partie"), 1, WHITE)
nouvellePartie_rect = nouvellePartie.get_rect()
nouvellePartie_rect.bottomleft = (150, 300)
maSurface.blit(nouvellePartie, nouvellePartie_rect)
chargerPartie = font.render(str("Charger une partie"), 1, WHITE)
chargerPartie_rect = chargerPartie.get_rect()
chargerPartie_rect.bottomleft = (200, 400)
maSurface.blit(chargerPartie, chargerPartie_rect)
pygame.display.update()

# Display menu before starting a game
while True:

    for event in pygame.event.get():
        quit(event)

        # Test mouse click
        if event.type == MOUSEBUTTONDOWN and event.button == 1:

            if nouvellePartie_rect.collidepoint(event.pos):
                notSaveGame = True
                pseudo = entrerPseudo()
                playgame(notSaveGame, Choix_list, pseudo)

            if chargerPartie_rect.collidepoint(event.pos):
                notSaveGame = False
                pseudo = entrerPseudo()
                playgame(notSaveGame, Choix_list, pseudo)