from initialisations import *
from tourDeJeu import *

nb_Color, len_Guess,nb_Try = var_Init()         #init game
Guess = init_Guess(nb_Color,len_Guess)          #random Guess

for tours in range(nb_Try):
    print("Essai : ", tours+1)                  #enieme tour
    Choix_list = Play(nb_Color,len_Guess)       #input players
    same, place = CheckPlay(Choix_list,Guess)   #calcul du résultat
    affichage_result(same,place)                #renvoie du résultat
    if same == len_Guess:                       #check if win
        break

if same != len_Guess:                           #check is loose
    affichage_guess(Guess)