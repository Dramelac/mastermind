import random

def var_Init():                                                                 #init var
    nb_Color = 0
    len_Guess = 0
    nb_Try = 0
    while nb_Color < 4 or nb_Color > 9:
        nb_Color = eval(input("Entrer le nombre de couleur : "))                #init color
    while len_Guess < 3 or len_Guess > 6:
        len_Guess = eval(input("Entrer la taille de la suite : "))              #init len
    while nb_Try < 5 or nb_Try > 15:
        nb_Try = eval(input("Entrer le nombre d'essais : "))                    #init try

    return nb_Color,len_Guess,nb_Try

def init_Guess(nb_Color,len_Guess):                                             #random Guess
    Guess = [0]*len_Guess
    for i in range(len_Guess):
        Guess[i] = random.randrange(1,nb_Color+1)

    return Guess